/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Empresa;
import entity.Tipousuario;
import entity.Usuario;
import model.EmpresaModel;
import model.UsuarioModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class EmpresaController {

    @RequestMapping(value = "getAlldp", method = RequestMethod.GET)
    public String getAlldp(Model m) {

        EmpresaModel model = new EmpresaModel();
        m.addAttribute("lst", model.getAll());
        return "datadp";
    }

    //remover
    @RequestMapping(value = "removedp", method = RequestMethod.GET)
    public String removedp(@RequestParam(value = "rut") String rut, Model m) {

        String bd = new String(rut);
        EmpresaModel model = new EmpresaModel();
        //get EMpleados bases objct id        m.addAttribute("lst",model.getAll());
        Empresa e = new Empresa();
        e = model.getEmpresa(bd);
        model.remove(e);
        // m.addAttribute("lst",e);       

        return "redirect:getAlldp.html";
    }

    //create
    @RequestMapping(value = "creardp", method = RequestMethod.GET)
    public String createdp(Model m) {
        //  java.math.BigDecimal bd=new java.math.BigDecimal(String.valueOf(id));
        Empresa dp = new Empresa();

        m.addAttribute("dp", dp);

        return "creardp";
    }

    @RequestMapping(value = "adddp", method = RequestMethod.POST)
    public String createdp(@ModelAttribute(value = "Empresa") Empresa p) {

        EmpresaModel model=new EmpresaModel();
        model.create(p);
        return "redirect:getAlldp.html";
    }

}
