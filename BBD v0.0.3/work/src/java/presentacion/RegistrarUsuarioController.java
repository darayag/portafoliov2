/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Duoc
 */
@WebServlet(name = "RegistrarUsuarioController", urlPatterns = {"/RegistrarUsuarioController"})
public class RegistrarUsuarioController extends HttpServlet {
    
    @EJB
    private HostelService service;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegistrarUsuarioController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegistrarUsuarioController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RegistroUsuarioDTO usuario = new RegistroUsuarioDTO();
        processRequest("usuario", usuario);
        
         request.getRequestDispatcher("/login.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         request.setCharacterEncoding("UTF-8");
        RegistroUsuarioDTO usuario = new RegistroUsuarioDTO();
        Map<String, String> mapMensajes = new HashMap<>();
        String mensaje;

        // convertir y validar
        usuario.setNombre(request.getParameter("nombre"));
        if (usuario.getNombre().isEmpty()) {
            mapMensajes.put("nombre", "Favor, Ingrese un nombre");
        }

        usuario.setApellido(request.getParameter("apellido"));
        if (usuario.getApellido().isEmpty()) {
            mapMensajes.put("apellido", "Favor, Ingrese un apellido");
        }

        usuario.setEmail(request.getParameter("email"));
        if (usuario.getEmail().isEmpty()) {
            mapMensajes.put("email", "Favor, Ingrese un email");
        }

        usuario.setPassword(request.getParameter("password"));
        if (usuario.getPassword().isEmpty()) {
            mapMensajes.put("password", "Favor, Ingrese una password");
        }

        // delegarla logica de negocio
        if (mapMensajes.isEmpty()) {
            try {

                service.agregarUsuario(usuario);
                mensaje = "Usuario agregado exitosamente";
            } catch (ServicioException ex) {

                mensaje = ex.getMessage();
            }
        } else {
            mensaje = "Favor, revise el formulario";
        }

        request.setAttribute("mapMensajes", mapMensajes);
        request.setAttribute("mensaje", mensaje);
        request.setAttribute("usuario", usuario);
        request.getRequestDispatcher("/login.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
