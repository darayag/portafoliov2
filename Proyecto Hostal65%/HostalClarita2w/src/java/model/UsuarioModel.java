/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Usuario;
import org.springframework.stereotype.Controller;
import java.util.ArrayList;
import java.util.List;
import model.HibernateUtil;
import org.hibernate.Session;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Diodoros
 */

@Controller
public class UsuarioModel {
    
       @RequestMapping()
    public List<Usuario> getAllUsuario() {
        List<Usuario> lst = new ArrayList<Usuario>();
        
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            s.beginTransaction();
            lst = s.createCriteria(Usuario.class).list();
            s.getTransaction().commit();

        } catch (Exception e) {

            e.printStackTrace();
        }
        
        return lst;
    }
    
    //creando crud
    
    //create
    public void create(Usuario u)
    {
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            s.beginTransaction();
            s.save(u);
            s.getTransaction().commit();

        } catch (Exception e) {

            e.printStackTrace();
              s.getTransaction().rollback();
        }
    }
    
    //remove
     public void remove(Usuario u)
    {
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            s.beginTransaction();
            s.delete(u);
            s.getTransaction().commit();

        } catch (Exception e) {

            e.printStackTrace();
              s.getTransaction().rollback();
        }
    }
     
     
     
     //update
      public void edit(Usuario u)
    {
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            s.beginTransaction();
            s.update(u);
            s.getTransaction().commit();

        } catch (Exception e) {

            e.printStackTrace();
              s.getTransaction().rollback();
        }
    }
      
     // other funtion
      
      
      public Usuario getUsuario(String correo){
          
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        Usuario u=new Usuario();
        
        try {
            s.beginTransaction();
            u=(Usuario) s.get(Usuario.class,correo);
            s.getTransaction().commit();

        } catch (Exception e) {

            e.printStackTrace();
            s.getTransaction().rollback();
        }
          
      
      return u;
      
      }
    
    
    
}
