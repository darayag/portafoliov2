/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Empresa;
import entity.Habitacion;
import entity.Huesped;
import entity.Tipohabitacion;
import java.math.BigDecimal;
import model.EmpresaModel;
import model.HabitacionModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;



/**
 *
 * @author Diodoros
 */
@Controller
public class HabitacionController {
       
    @RequestMapping(value="getAllhb",method = RequestMethod.GET)
    public String getAll(Model m)
    {
        
        HabitacionModel model= new HabitacionModel();
        m.addAttribute("lst",model.getAll());
        return "datahb";
    }
    
    @RequestMapping(value="edithb",method = RequestMethod.GET)
    public String edit(@RequestParam(value="idhabitacion") int idhabitacion,Model m)
    {
        java.math.BigDecimal bd=new java.math.BigDecimal(String.valueOf(idhabitacion));
        HabitacionModel model= new HabitacionModel();
       
        Habitacion h =new Habitacion();
        Empresa hb=new Empresa();
        EmpresaModel hbModel= new EmpresaModel();
        h=model.getHabitacion(bd);
        hb=hbModel.getEmpresa(h.getEmpresa().getRut());
        
        m.addAttribute("h",h);
        //m.addAttribute("d", dp);
      //  m.addAttribute("d",p.getDepartamento());
        
        return "edithb";
    }
    
    @RequestMapping(value = "updatehb",method=RequestMethod.POST)
    public String update(@ModelAttribute(value="Habitacion") Habitacion h)
    {

        
       HabitacionModel model= new HabitacionModel();
       EmpresaModel dpmodel=new EmpresaModel();
       Empresa dp=new Empresa();
       Habitacion aux= new Habitacion();
       aux=model.getHabitacion(h.getIdhabitacion());
       aux.setEstadoha(h.getEstadoha());

        
           // aux.setDepartamento(dp);
        model.edit(aux);
        return "redirect:getAllhb.html";
    }

    //remover
    
    @RequestMapping(value = "removehb", method = RequestMethod.GET)
    public String remove(@RequestParam(value = "idhabitacion") int idhabitacion, Model m) {

        java.math.BigDecimal bd = new java.math.BigDecimal(String.valueOf(idhabitacion));
        HabitacionModel model = new HabitacionModel();
        Habitacion e = new Habitacion();
        e = model.getHabitacion(bd);
        model.remove(e);
                    

        return "redirect:getAllhb.html";
    }
    
    
     @RequestMapping(value="crearhb",method = RequestMethod.GET)
    public String create(Model m)
    {
        Habitacion hb = new Habitacion();

        m.addAttribute("hb", hb);

        return "crearhb";
    }
    
    
    
    @RequestMapping(value = "addhb",method=RequestMethod.POST)
    public String create(@ModelAttribute(value="Habitacion") Habitacion h)
    {

        HabitacionModel model=new HabitacionModel();
        model.create(h);
        return "redirect:getAllhb.html";
        
        
    }
   
 
}
