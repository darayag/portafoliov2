/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import entity.Proveedor;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Diodoros
 */
public class ProveedorModel {
     //read
    @RequestMapping()
    public List<Proveedor> getAll() {
        List<Proveedor> lst = new ArrayList<Proveedor>();
        
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            s.beginTransaction();
            lst = s.createCriteria(Proveedor.class).list();
            s.getTransaction().commit();

        } catch (Exception e) {

            e.printStackTrace();
        }
        
        return lst;
    }
    
    
    //creando crud
    
    //create
    public void create(Proveedor pr)
    {
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            s.beginTransaction();
            s.save(pr);
            s.getTransaction().commit();

        } catch (Exception e) {

            e.printStackTrace();
              s.getTransaction().rollback();
        }
    }
    
    //remove
     public void remove(Proveedor pr)
    {
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            s.beginTransaction();
            s.delete(pr);
            s.getTransaction().commit();

        } catch (Exception e) {

            e.printStackTrace();
              s.getTransaction().rollback();
        }
    }
     
     
     
     //update
      public void edit(Proveedor pr)
    {
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            s.beginTransaction();
            s.update(pr);
            s.getTransaction().commit();

        } catch (Exception e) {

            e.printStackTrace();
              s.getTransaction().rollback();
        }
    }
      
     // other funtion
      
      
      public Proveedor getEmpleado(String idempleado){
          
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        Proveedor p=new Proveedor();
        
        try {
            s.beginTransaction();
            p=(Proveedor) s.get(Proveedor.class,idempleado);
            s.getTransaction().commit();

        } catch (Exception e) {

            e.printStackTrace();
            s.getTransaction().rollback();
        }
          
      
      return p;
      
      }
    
    
}
