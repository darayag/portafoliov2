<%-- 
    Document   : crearcm
    Created on : 23-05-2019, 23:38:32
    Author     : Diodoros
--%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>



<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/fav.png">
        <!-- Author Meta -->
        <meta name="author" content="CodePixar">
        <!-- Meta Description -->
        <meta name="description" content="">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->
        <title>Comedor</title>
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,500,600" rel="stylesheet">
        <!--
        CSS
        ============================================= -->
        <link rel="stylesheet" href="css/linearicons.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <div class="oz-body-wrap">
            <!-- Start Header Area -->
            <header class="default-header generic-header">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <div class="header-top d-flex justify-content-between align-items-center">
                            <div class="logo">
                                <a href="index.jsp"><img src="img/logo.png" alt=""></a>
                            </div>
                            <div class="main-menubar d-flex align-items-center">
                                <nav class="hide">
                                   <a href="getAll.html">Lista de Usuarios  </a>
                                    <a href="getAllem.html">Lista de Empleados </a>
                                    <a href="getAlldp.html">Lista de Empresas </a>
                                     <a href="getAllpr.html">Lista de Proveedores </a>
                                    <a href="getAllhb.html">Lista de Habitaciones </a> 
                                    <a href="getAllcm.html">Lista de Comedor </a>
                                </nav>
                                <div class="menu-bar"><span class="lnr lnr-menu"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- End Header Area -->
            <!-- Start Banner Area -->
            <section class="generic-banner relative">
                <div class="overlay overlay-bg"></div>
                <div class="container">
                    <div class="row height align-items-center justify-content-center">
                        <div class="col-lg-10">
                            <div class="banner-content text-center">

                                <br>
                                <div class="container">
                                    <center>
                                        <br>
                                        <h1>Agregar Comedor</h1>
                                        <br><br>
                                        <f:form action="addcm.html" modelAttribute="Comedor">
                                            <center>
                                                
                                                <label for="idcomedor" >ID Comedor :</label>
                                                
                                                <input type="text" name="idcomedor" id="idcomedorid" value=""  class="form-control"/> 
                                                
                                                <label for="desayunoid" >Desayuno :</label>
                                                
                                                <input type="text" name="desayuno" id="desayunoid" value=""  class="form-control"/> 
                                                
                                                <label for="almuerzoid" >Almuerzo :</label>
                                                
                                                <input type="text" name="almuerzo" id="almuerzoid" value=""  class="form-control"/> 
                                                
                                                <label for="cenaid" >Cena :</label>
                                                
                                                <input type="text" name="cena" id="cenaid" value=""  class="form-control"/> 
                                                
                                                
                                                <select  class="form-control" style="width: 250px;" id="diasemana_diaidid" value="" name="diasemana.diaid" >
                                                    <option>Seleccione Dia de la Semana</option>
                                                    <%

                                                        try {
                                                            String Query = "select * from diasemana";
                                                            Class.forName("oracle.jdbc.OracleDriver").newInstance();
                                                            Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "C##PORTA", "oracle");
                                                            Statement stm = con.createStatement();
                                                            ResultSet rs = stm.executeQuery(Query);
                                                            while (rs.next()) {
                                                    %>
                                                    <option value="<%=rs.getInt("diaid")%>"><%=rs.getString("nombredia")%></option>
                                                    <%
                                                            }

                                                        } catch (Exception ex) {
                                                            ex.printStackTrace();
                                                            out.print("Error " + ex.getMessage());
                                                        }


                                                    %>
                                                </select>

                                                <br>
                                                <br>
                                                <select  class="form-control" style="width: 250px;" id="empleado_idempleadoid" value="" name="empleado.idempleado" >
                                                    <option>Selecciona  Empleado </option>
                                                    <%                    try {
                                                            String Query = "select * from empleado";
                                                            Class.forName("oracle.jdbc.OracleDriver").newInstance();
                                                            Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "C##PORTA", "oracle");
                                                            Statement stm = con.createStatement();
                                                            ResultSet rs = stm.executeQuery(Query);
                                                            while (rs.next()) {
                                                    %>
                                                    <option value="<%=rs.getString("idempleado")%>"><%=rs.getString("nombre")%></option>
                                                    <%
                                                            }

                                                        } catch (Exception ex) {
                                                            ex.printStackTrace();
                                                            out.print("Error " + ex.getMessage());
                                                        }


                                                    %>
                                                </select>

                                                
                                                <br>
                                                <select  class="form-control" style="width: 250px;" id="tiposervicio_numservid" value="" name="tiposervicio.numserv" >
                                                    <option>Selecciona  Servicio</option>
                                                    <%                    try {
                                                            String Query = "select * from tiposervicio";
                                                            Class.forName("oracle.jdbc.OracleDriver").newInstance();
                                                            Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "C##PORTA", "oracle");
                                                            Statement stm = con.createStatement();
                                                            ResultSet rs = stm.executeQuery(Query);
                                                            while (rs.next()) {
                                                    %>
                                                    <option value="<%=rs.getInt("numserv")%>"><%=rs.getString("nomserv")%></option>
                                                    <%
                                                            }

                                                        } catch (Exception ex) {
                                                            ex.printStackTrace();
                                                            out.print("Error " + ex.getMessage());
                                                        }


                                                    %>
                                                </select>

                                                <br>

                                                <input type="submit" value="Save" class="btn btn-primary btn-lg"/> <a href="http://localhost:8080/HostalClarita/getAllcm.html" class="btn btn-primary btn-lg"/>Regresar</a>

                                            </center>
                                        </f:form>

                                    </center>
                                </div>
                                <br>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Start Feature Area -->

            <!-- Start Feature Area -->
            <!-- About Generic Start -->

            <!-- Start Service Area -->

            <!-- End Service Area -->
            <!-- Start Footer Area -->

            <!-- End Footer Area -->

        </div>
        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>