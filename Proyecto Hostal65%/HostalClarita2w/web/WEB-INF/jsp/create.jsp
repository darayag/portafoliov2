<%-- 
    Document   : create
    Created on : 16-05-2019, 1:18:14
    Author     : Diodoros
--%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>



<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/fav.png">
        <!-- Author Meta -->
        <meta name="author" content="CodePixar">
        <!-- Meta Description -->
        <meta name="description" content="">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->
        <title> Usuarios</title>
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,500,600" rel="stylesheet">
        <!--
        CSS
        ============================================= -->
        <link rel="stylesheet" href="css/linearicons.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <div class="oz-body-wrap">
            <!-- Start Header Area -->
            <header class="default-header generic-header">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <div class="header-top d-flex justify-content-between align-items-center">
                            <div class="logo">
                                <a href="index.jsp"><img src="img/logo.png" alt=""></a>
                            </div>
                            <div class="main-menubar d-flex align-items-center">
                                <nav class="hide">
                                    <a href="getAll.html">Lista de Usuarios  </a>
                                    <a href="getAllem.html">Lista de Empleados </a>
                                    <a href="getAlldp.html">Lista de Empresas </a>
                                     <a href="getAllpr.html">Lista de Proveedores </a>
                                    <a href="getAllhb.html">Lista de Habitaciones </a> 
                                    <a href="getAllcm.html">Lista de Comedor </a>
                                </nav>
                                <div class="menu-bar"><span class="lnr lnr-menu"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- End Header Area -->
            <!-- Start Banner Area -->
            <section class="generic-banner relative">
                <div class="overlay overlay-bg"></div>
                <div class="container">
                    <div class="row height align-items-center justify-content-center">
                        <div class="col-lg-10">
                            <div class="banner-content text-center">

                                <br>
                                <div class="container">
                                    <center>

                                        <h1>Agregar Usuario</h1><br>
                                        <br><br><br><br><br>
                                        <f:form action="add.html" modelAttribute="Usuario">
                                            <form action="" method="post" class="form-control" stlye="width: 500px; heigth: 400px;">

                                                <label for="correoid">Correo:</label>

                                                <input type="email" id="correoid"name="correo" value="" class="form-control"/>


                                                <label for="passid">Contraseña:</label>

                                                <input type="password" id="passid"name="pass" value="" class="form-control"/>
                                                <br>



                                                <select  class="form-control" style="width: 250px;" id="usuario_correoid" value="" name="tipousuario.iduser" >
                                                    <option>Seleccione Usuario</option>

                                                    <%

                                                        try {
                                                            String Query = "select * from tipousuario";
                                                            Class.forName("oracle.jdbc.OracleDriver").newInstance();
                                                            Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "C##PORTA", "oracle");
                                                            Statement stm = con.createStatement();
                                                            ResultSet rs = stm.executeQuery(Query);
                                                            while (rs.next()) {
                                                    %>
                                                    <option value="<%=rs.getString("iduser")%>"><%=rs.getString("descripcion")%></option>
                                                    <%
                                                            }

                                                        } catch (Exception ex) {
                                                            ex.printStackTrace();
                                                            out.print("Error " + ex.getMessage());
                                                        }


                                                    %>

                                                </select>
                                                <br><br> <br>
                                                <input type="submit" value="Guardar" class="btn btn-primary btn-lg"/>                            <a href="http://localhost:8080/HostalClarita/getAll.html" class="btn btn-primary btn-lg">Regresar</a> 
                                                <br>
                                            </form>
                                          
                                        </f:form>

                                    </center>
                                </div>
                                <br>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Start Feature Area -->

            <!-- Start Feature Area -->
            <!-- About Generic Start -->

            <!-- Start Service Area -->

            <!-- End Service Area -->
            <!-- Start Footer Area -->

            <!-- End Footer Area -->

        </div>
        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>





