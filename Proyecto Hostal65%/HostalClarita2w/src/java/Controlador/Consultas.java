/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;


import java.math.BigInteger;
import java.sql.Connection;
import java.util.LinkedList;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;

/**
 *
 * @author the_e
 */
public class Consultas extends Conexion{
    
    static Connection conn;
    
    
    //Email - Pass
    public boolean autenticacion(String correo, String pass) {
        OraclePreparedStatement pst = null;
        OracleResultSet rs = null;
        conn = Conexion.conexion();
        try {
            String consulta = "select * from usuario where correo = ? and pass = ?";
            pst = (OraclePreparedStatement) conn.prepareStatement(consulta);
            pst.setString(1, correo);
            pst.setString(2, pass);
            rs = (OracleResultSet) pst.executeQuery();

            if (rs.next()) {
                return true;
            }

        } catch (Exception e) {
            System.err.println("Error" + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.err.println("Error" + e);
            }
        }
        return false;
    }
    

    
}
