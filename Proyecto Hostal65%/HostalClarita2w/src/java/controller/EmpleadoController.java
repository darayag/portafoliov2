/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Empleado;
import model.EmpleadoModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Diodoros
 */
@Controller
public class EmpleadoController {
 
    @RequestMapping(value = "getAllem", method = RequestMethod.GET)
    public String getAllem(Model m) {

        EmpleadoModel model = new EmpleadoModel();
        m.addAttribute("lst", model.getAll());
        return "dataem";
    }

    //remover
    @RequestMapping(value = "removeem", method = RequestMethod.GET)
    public String removedp(@RequestParam(value = "idempleado") String idempleado, Model m) {

        String bd = new String(idempleado);
        EmpleadoModel model = new EmpleadoModel();
        //get EMpleados bases objct id        m.addAttribute("lst",model.getAll());
        Empleado e = new Empleado();
        e = model.getEmpleado(bd);
        model.remove(e);
        // m.addAttribute("lst",e);       

        return "redirect:getAllem.html";
    }

    //create
    @RequestMapping(value = "crearem", method = RequestMethod.GET)
    public String createdp(Model m) {
        //  java.math.BigDecimal bd=new java.math.BigDecimal(String.valueOf(id));
        Empleado dp = new Empleado();

        m.addAttribute("dp", dp);

        return "crearem";
    }

    @RequestMapping(value = "addem", method = RequestMethod.POST)
    public String createdp(@ModelAttribute(value = "Empleado") Empleado dp) {

        EmpleadoModel model = new EmpleadoModel();
        model.create(dp);
        return "redirect:getAllem.html";
    }

}
