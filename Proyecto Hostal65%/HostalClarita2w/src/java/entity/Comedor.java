package entity;
// Generated 15-05-2019 22:13:08 by Hibernate Tools 4.3.1


import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Comedor generated by hbm2java
 */
@Entity
@Table(name="COMEDOR"
    ,schema="C##PORTA"
    , uniqueConstraints = @UniqueConstraint(columnNames="DIASEMANA_DIAID") 
)
public class Comedor  implements java.io.Serializable {


     private BigDecimal idcomedor;
     private Diasemana diasemana;
     private Empleado empleado;
     private Tiposervicio tiposervicio;
     private String desayuno;
     private String almuerzo;
     private String cena;

    public Comedor() {
    }

    public Comedor(BigDecimal idcomedor, Diasemana diasemana, Empleado empleado, Tiposervicio tiposervicio, String desayuno, String almuerzo, String cena) {
       this.idcomedor = idcomedor;
       this.diasemana = diasemana;
       this.empleado = empleado;
       this.tiposervicio = tiposervicio;
       this.desayuno = desayuno;
       this.almuerzo = almuerzo;
       this.cena = cena;
    }
   
     @Id 

    
    @Column(name="IDCOMEDOR", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdcomedor() {
        return this.idcomedor;
    }
    
    public void setIdcomedor(BigDecimal idcomedor) {
        this.idcomedor = idcomedor;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="DIASEMANA_DIAID", unique=true, nullable=false)
    public Diasemana getDiasemana() {
        return this.diasemana;
    }
    
    public void setDiasemana(Diasemana diasemana) {
        this.diasemana = diasemana;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="EMPLEADO_IDEMPLEADO", nullable=false)
    public Empleado getEmpleado() {
        return this.empleado;
    }
    
    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="TIPOSERVICIO_NUMSERV", nullable=false)
    public Tiposervicio getTiposervicio() {
        return this.tiposervicio;
    }
    
    public void setTiposervicio(Tiposervicio tiposervicio) {
        this.tiposervicio = tiposervicio;
    }

    
    @Column(name="DESAYUNO", nullable=false, length=192)
    public String getDesayuno() {
        return this.desayuno;
    }
    
    public void setDesayuno(String desayuno) {
        this.desayuno = desayuno;
    }

    
    @Column(name="ALMUERZO", nullable=false, length=192)
    public String getAlmuerzo() {
        return this.almuerzo;
    }
    
    public void setAlmuerzo(String almuerzo) {
        this.almuerzo = almuerzo;
    }

    
    @Column(name="CENA", nullable=false, length=192)
    public String getCena() {
        return this.cena;
    }
    
    public void setCena(String cena) {
        this.cena = cena;
    }




}


