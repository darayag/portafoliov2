<%-- 
    Document   : login
    Created on : 03-may-2019, 12:49:28
    Author     : Duoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html >
    <head>
        <meta charset="UTF-8">
        <title>Hostel "Clarita" | Inicio de sesión</title>
        <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">


        <link rel="stylesheet" href="CSSLogin/css/style.css">
    </head>

    <body>
        <div class="form">

            <ul class="tab-group">
                <li class="tab active"><a href="#signup">Regístrate</a></li>
                <li class="tab"><a href="#login">Iniciar sesión</a></li>
            </ul>

            <div class="tab-content">
                <div id="signup">   
                    <h1>Regístrate gratis</h1>

                    <form action="<c:url value="/RegistrarUsuarioController" />" method="post">
                          <div class="top-row">
                            <div class="field-wrap">
                                <label>
                                    Nombre<span class="req">*</span>
                                </label>
                                <input type="text" required autocomplete="off" name="nombre" value=""/>
                                <c:out value="${mapMensajes['nombre']}"/>
                            </div>
                             
                            <div class="field-wrap">
                                <label>
                                    Apellido<span class="req">*</span>
                                </label>
                                <input type="text" required autocomplete="off" name="apellido" value=""/>
                                <c:out value="${mapMensajes['apellido']}"/>
                            </div>
                        </div>
                            
                         
 
                            
                        <div class="field-wrap">
                            <label>
                                rut<span class="req">*</span>
                            </label>
                            <input type="text" required autocomplete="off" name="rut" value=""/>
                            <c:out value="${mapMensajes['rut']}"/>
                        </div>

                        <div class="field-wrap">
                            <label>
                                Establecer una contraseña<span class="req">*</span>
                            </label>
                            <input type="password" required autocomplete="off" name="password" value=""/>
                            <c:out value="${mapMensajes['password']}"/>
                        </div>

                        <button type="submit" class="button button-block"/>Comenzar</button>
                        <c:out value="${mensaje}"/>

                    </form>

                </div>

                <div id="login">   
                    <h1>¡Bienvenido!</h1>

                    <form action="Iniciar" method="post">

                        <div class="field-wrap">
                            <label>
                                Correo electrónico<span class="req">*</span>
                            </label>
                            <input type="email"required autocomplete="off" name="email"/>
                        </div>

                        <div class="field-wrap">
                            <label>
                                Contraseña<span class="req">*</span>
                            </label>
                            <input type="password"required autocomplete="off" name="password"/>
                        </div>

                        <p class="forgot"><a href="#">Recuperar contraseña</a></p>

                        <button class="button button-block"/>Iniciar sesión</button>

                    </form>

                </div>

            </div><!-- tab-content -->

        </div> <!-- /form -->
        <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script  src="CSSLogin/js/index.js"></script>

    </body>
</html>