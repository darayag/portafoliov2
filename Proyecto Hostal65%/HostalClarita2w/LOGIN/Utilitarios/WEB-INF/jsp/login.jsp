<%-- 
    Document   : Login
    Created on : 28/11/2016, 05:32:38 PM
    Author     : undercoder
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inicio Session</title>                        
    </head>


    <body>

        <div class="login-form">
            <h1>Prueba</h1>
            <div class="form-group ">
                <form name='loginForm' action="<c:url value='j_spring_security_check'/>" method='POST'>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> 
                    <input type="email" class="form-control" placeholder="email" id="username" name="username">
                    <i class="fa fa-user"></i>
            </div>
            <div class="form-group log-status">
                <input type="password" class="form-control" placeholder="password" id="password" name="password">
                <i class="fa fa-lock"></i>
            </div>
          
            <center>  <input class="log-btn" name="submit" type="submit" value="Entrar"/></center>
        </form>
    </div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
   
</body>
</html>
