/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Huesped;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Diodoros
 */
public class HuespedModel {
    //read

    @RequestMapping()
    public List<Huesped> getAll() {
        List<Huesped> lst = new ArrayList<Huesped>();

        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            s.beginTransaction();
            lst = s.createCriteria(Huesped.class).list();
            s.getTransaction().commit();

        } catch (Exception e) {

            e.printStackTrace();
        }

        return lst;
    }

    //creando crud
    //create
    public void create(Huesped hu) {
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            s.beginTransaction();
            s.save(hu);
            s.getTransaction().commit();

        } catch (Exception e) {

            e.printStackTrace();
            s.getTransaction().rollback();
        }
    }

    //remove
    public void remove(Huesped hu) {
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            s.beginTransaction();
            s.delete(hu);
            s.getTransaction().commit();

        } catch (Exception e) {

            e.printStackTrace();
            s.getTransaction().rollback();
        }
    }

    //update
    public void edit(Huesped hu) {
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            s.beginTransaction();
            s.update(hu);
            s.getTransaction().commit();

        } catch (Exception e) {

            e.printStackTrace();
            s.getTransaction().rollback();
        }
    }

    // other funtion
    public Huesped getHuesped(String ruth) {

        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        Huesped hu = new Huesped();

        try {
            s.beginTransaction();
            hu = (Huesped) s.get(Huesped.class, ruth);
            s.getTransaction().commit();

        } catch (Exception e) {

            e.printStackTrace();
            s.getTransaction().rollback();
        }

        return hu;

    }

}
