--------------------------------------------------------
-- Archivo creado  - martes-abril-16-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence FACTURA_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "C##PORTA"."FACTURA_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 4 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence HABITACION_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "C##PORTA"."HABITACION_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 24 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence ORDENCOMPRA_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "C##PORTA"."ORDENCOMPRA_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 24 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Table COMEDOR
--------------------------------------------------------

  CREATE TABLE "C##PORTA"."COMEDOR" 
   (	"IDCOMEDOR" NUMBER(*,0), 
	"DESAYUNO" VARCHAR2(20 CHAR), 
	"ALMUERZO" VARCHAR2(20 CHAR), 
	"CENA" VARCHAR2(20 CHAR), 
	"DIASEMANA_NUMDIA" NUMBER(*,0), 
	"TIPOSERVICIO_NUMSERV" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table DIASEMANA
--------------------------------------------------------

  CREATE TABLE "C##PORTA"."DIASEMANA" 
   (	"NUMDIA" NUMBER(*,0), 
	"NOMDIA" VARCHAR2(10 CHAR)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table FACTURA
--------------------------------------------------------

  CREATE TABLE "C##PORTA"."FACTURA" 
   (	"NUMFACTURA" NUMBER(*,0), 
	"ORDENCOMPRA_NUMORDEN" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table HABITACION
--------------------------------------------------------

  CREATE TABLE "C##PORTA"."HABITACION" 
   (	"IDHABITACION" NUMBER(*,0), 
	"ESTADOHA" NUMBER(*,0), 
	"TIPOHABITACION_TIPO" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table HUESPED
--------------------------------------------------------

  CREATE TABLE "C##PORTA"."HUESPED" 
   (	"RUTH" VARCHAR2(10 CHAR), 
	"NOMBREH" VARCHAR2(48 CHAR), 
	"HABITACION_IDHABITACION" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table ORDENCOMPRA
--------------------------------------------------------

  CREATE TABLE "C##PORTA"."ORDENCOMPRA" 
   (	"NUMORDEN" NUMBER(*,0), 
	"CANTIDAD" NUMBER(*,0), 
	"PRECIOTOTAL" NUMBER(*,0), 
	"COSTOTOTAL" NUMBER(*,0), 
	"HABITACION_IDHABITACION" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table PRODUCTO
--------------------------------------------------------

  CREATE TABLE "C##PORTA"."PRODUCTO" 
   (	"ID" NUMBER(*,0), 
	"FECHAV" DATE, 
	"DESCRIPCION" VARCHAR2(48 CHAR), 
	"PRECIOP" NUMBER(*,0), 
	"STOCK" NUMBER(*,0), 
	"STOCKCRITICO" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table TIPOHABITACION
--------------------------------------------------------

  CREATE TABLE "C##PORTA"."TIPOHABITACION" 
   (	"TIPO" NUMBER(*,0), 
	"NOMBREHA" VARCHAR2(40 CHAR), 
	"PRECIO" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table TIPOSERVICIO
--------------------------------------------------------

  CREATE TABLE "C##PORTA"."TIPOSERVICIO" 
   (	"NUMSERV" NUMBER(*,0), 
	"NOMSERV" VARCHAR2(15 CHAR)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table TIPOUSUARIO
--------------------------------------------------------

  CREATE TABLE "C##PORTA"."TIPOUSUARIO" 
   (	"TIPOUSER" NUMBER(*,0), 
	"NOMTIPO" VARCHAR2(18 CHAR)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table USUARIO
--------------------------------------------------------

  CREATE TABLE "C##PORTA"."USUARIO" 
   (	"RUT" VARCHAR2(10 CHAR), 
	"NOMBRE" VARCHAR2(48 CHAR), 
	"PASS" VARCHAR2(20 CHAR), 
	"EMAIL" VARCHAR2(48 CHAR), 
	"DIRECCION" VARCHAR2(48 CHAR), 
	"CIUDAD" VARCHAR2(48 CHAR), 
	"TELEFONO" VARCHAR2(12 CHAR), 
	"TIPOUSUARIO_TIPOUSER" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into C##PORTA.COMEDOR
SET DEFINE OFF;
Insert into C##PORTA.COMEDOR (IDCOMEDOR,DESAYUNO,ALMUERZO,CENA,DIASEMANA_NUMDIA,TIPOSERVICIO_NUMSERV) values ('1','CAFE Y TOSTADAS','CAZUELA','POLLO CON VERDURAS','1','0');
Insert into C##PORTA.COMEDOR (IDCOMEDOR,DESAYUNO,ALMUERZO,CENA,DIASEMANA_NUMDIA,TIPOSERVICIO_NUMSERV) values ('2','YOGURTH','POLLO CON PAPAS','PIZZA','2','2');
Insert into C##PORTA.COMEDOR (IDCOMEDOR,DESAYUNO,ALMUERZO,CENA,DIASEMANA_NUMDIA,TIPOSERVICIO_NUMSERV) values ('3','TECITO CON PAN','FIDEOS AL PESTO','CHOP SUEY','3','0');
REM INSERTING into C##PORTA.DIASEMANA
SET DEFINE OFF;
Insert into C##PORTA.DIASEMANA (NUMDIA,NOMDIA) values ('1','LUNES');
Insert into C##PORTA.DIASEMANA (NUMDIA,NOMDIA) values ('2','MARTES');
Insert into C##PORTA.DIASEMANA (NUMDIA,NOMDIA) values ('3','MIERCOLES');
Insert into C##PORTA.DIASEMANA (NUMDIA,NOMDIA) values ('4','JUEVES');
Insert into C##PORTA.DIASEMANA (NUMDIA,NOMDIA) values ('5','VIERNES');
Insert into C##PORTA.DIASEMANA (NUMDIA,NOMDIA) values ('6','SABADO');
Insert into C##PORTA.DIASEMANA (NUMDIA,NOMDIA) values ('7','DOMINGO');
REM INSERTING into C##PORTA.FACTURA
SET DEFINE OFF;
Insert into C##PORTA.FACTURA (NUMFACTURA,ORDENCOMPRA_NUMORDEN) values ('0','1');
Insert into C##PORTA.FACTURA (NUMFACTURA,ORDENCOMPRA_NUMORDEN) values ('1','2');
Insert into C##PORTA.FACTURA (NUMFACTURA,ORDENCOMPRA_NUMORDEN) values ('2','3');
REM INSERTING into C##PORTA.HABITACION
SET DEFINE OFF;
Insert into C##PORTA.HABITACION (IDHABITACION,ESTADOHA,TIPOHABITACION_TIPO) values ('1','1','0');
Insert into C##PORTA.HABITACION (IDHABITACION,ESTADOHA,TIPOHABITACION_TIPO) values ('2','1','1');
Insert into C##PORTA.HABITACION (IDHABITACION,ESTADOHA,TIPOHABITACION_TIPO) values ('3','1','1');
Insert into C##PORTA.HABITACION (IDHABITACION,ESTADOHA,TIPOHABITACION_TIPO) values ('4','1','2');
Insert into C##PORTA.HABITACION (IDHABITACION,ESTADOHA,TIPOHABITACION_TIPO) values ('5','1','1');
Insert into C##PORTA.HABITACION (IDHABITACION,ESTADOHA,TIPOHABITACION_TIPO) values ('6','1','1');
Insert into C##PORTA.HABITACION (IDHABITACION,ESTADOHA,TIPOHABITACION_TIPO) values ('7','1','1');
Insert into C##PORTA.HABITACION (IDHABITACION,ESTADOHA,TIPOHABITACION_TIPO) values ('8','1','1');
Insert into C##PORTA.HABITACION (IDHABITACION,ESTADOHA,TIPOHABITACION_TIPO) values ('9','1','1');
Insert into C##PORTA.HABITACION (IDHABITACION,ESTADOHA,TIPOHABITACION_TIPO) values ('10','1','1');
Insert into C##PORTA.HABITACION (IDHABITACION,ESTADOHA,TIPOHABITACION_TIPO) values ('11','1','1');
Insert into C##PORTA.HABITACION (IDHABITACION,ESTADOHA,TIPOHABITACION_TIPO) values ('12','1','1');
REM INSERTING into C##PORTA.HUESPED
SET DEFINE OFF;
Insert into C##PORTA.HUESPED (RUTH,NOMBREH,HABITACION_IDHABITACION) values ('13','Michael Saavedra','1');
Insert into C##PORTA.HUESPED (RUTH,NOMBREH,HABITACION_IDHABITACION) values ('4','Efren Mu�oz','1');
Insert into C##PORTA.HUESPED (RUTH,NOMBREH,HABITACION_IDHABITACION) values ('5','Efren Mu�oz','2');
Insert into C##PORTA.HUESPED (RUTH,NOMBREH,HABITACION_IDHABITACION) values ('22','Dario Araya','4');
REM INSERTING into C##PORTA.ORDENCOMPRA
SET DEFINE OFF;
Insert into C##PORTA.ORDENCOMPRA (NUMORDEN,CANTIDAD,PRECIOTOTAL,COSTOTOTAL,HABITACION_IDHABITACION) values ('1','1','14990','14990','1');
Insert into C##PORTA.ORDENCOMPRA (NUMORDEN,CANTIDAD,PRECIOTOTAL,COSTOTOTAL,HABITACION_IDHABITACION) values ('2','1','14990','29990','4');
Insert into C##PORTA.ORDENCOMPRA (NUMORDEN,CANTIDAD,PRECIOTOTAL,COSTOTOTAL,HABITACION_IDHABITACION) values ('3','1','14990','19990','2');
Insert into C##PORTA.ORDENCOMPRA (NUMORDEN,CANTIDAD,PRECIOTOTAL,COSTOTOTAL,HABITACION_IDHABITACION) values ('4','2','14990','29980','3');
Insert into C##PORTA.ORDENCOMPRA (NUMORDEN,CANTIDAD,PRECIOTOTAL,COSTOTOTAL,HABITACION_IDHABITACION) values ('5','2','14990','29980','3');
REM INSERTING into C##PORTA.PRODUCTO
SET DEFINE OFF;
Insert into C##PORTA.PRODUCTO (ID,FECHAV,DESCRIPCION,PRECIOP,STOCK,STOCKCRITICO) values ('1',to_date('09/09/19','DD/MM/RR'),'SIRVE PARA FREIR','600','999','0');
Insert into C##PORTA.PRODUCTO (ID,FECHAV,DESCRIPCION,PRECIOP,STOCK,STOCKCRITICO) values ('2',to_date('03/12/22','DD/MM/RR'),'LA MAS PURA DE LA CORDILLERA','1500','400','0');
Insert into C##PORTA.PRODUCTO (ID,FECHAV,DESCRIPCION,PRECIOP,STOCK,STOCKCRITICO) values ('3',to_date('01/01/20','DD/MM/RR'),'DESCRIPCION','1800','10','1');
Insert into C##PORTA.PRODUCTO (ID,FECHAV,DESCRIPCION,PRECIOP,STOCK,STOCKCRITICO) values ('4',to_date('01/01/20','DD/MM/RR'),'DESCRIPCION','0','0','1');
REM INSERTING into C##PORTA.TIPOHABITACION
SET DEFINE OFF;
Insert into C##PORTA.TIPOHABITACION (TIPO,NOMBREHA,PRECIO) values ('0','SENCILLO','14990');
Insert into C##PORTA.TIPOHABITACION (TIPO,NOMBREHA,PRECIO) values ('1','MATRIMONIAL','19990');
Insert into C##PORTA.TIPOHABITACION (TIPO,NOMBREHA,PRECIO) values ('2','SUITE','29990');
REM INSERTING into C##PORTA.TIPOSERVICIO
SET DEFINE OFF;
Insert into C##PORTA.TIPOSERVICIO (NUMSERV,NOMSERV) values ('0','GENERAL');
Insert into C##PORTA.TIPOSERVICIO (NUMSERV,NOMSERV) values ('1','EJECUTIVO');
Insert into C##PORTA.TIPOSERVICIO (NUMSERV,NOMSERV) values ('2','ESPECIAL');
REM INSERTING into C##PORTA.TIPOUSUARIO
SET DEFINE OFF;
Insert into C##PORTA.TIPOUSUARIO (TIPOUSER,NOMTIPO) values ('0','ADMINISTRADOR');
Insert into C##PORTA.TIPOUSUARIO (TIPOUSER,NOMTIPO) values ('1','CLIENTE');
Insert into C##PORTA.TIPOUSUARIO (TIPOUSER,NOMTIPO) values ('2','EMPLEADO');
Insert into C##PORTA.TIPOUSUARIO (TIPOUSER,NOMTIPO) values ('3','PROVEEDOR');
REM INSERTING into C##PORTA.USUARIO
SET DEFINE OFF;
Insert into C##PORTA.USUARIO (RUT,NOMBRE,PASS,EMAIL,DIRECCION,CIUDAD,TELEFONO,TIPOUSUARIO_TIPOUSER) values ('20618788-5','SKTT1','FEIQUER','FEIQUER@RIOTGAMES.COM','AV.RITO','BEIJING','98889666','1');
Insert into C##PORTA.USUARIO (RUT,NOMBRE,PASS,EMAIL,DIRECCION,CIUDAD,TELEFONO,TIPOUSUARIO_TIPOUSER) values ('19619188-7','Dario','Dario123','Darioxxq@gmail.com','Serena 824','Quilpue','977280449','0');
Insert into C##PORTA.USUARIO (RUT,NOMBRE,PASS,EMAIL,DIRECCION,CIUDAD,TELEFONO,TIPOUSUARIO_TIPOUSER) values ('19339876-6','Efren','Efren123','efrensotex@gmail.com','Lachucha 666','Vi�a del Mar','975453874','0');
--------------------------------------------------------
--  DDL for Index COMEDOR_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "C##PORTA"."COMEDOR_PK" ON "C##PORTA"."COMEDOR" ("IDCOMEDOR") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index DIASEMANA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "C##PORTA"."DIASEMANA_PK" ON "C##PORTA"."DIASEMANA" ("NUMDIA") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index FACTURA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "C##PORTA"."FACTURA_PK" ON "C##PORTA"."FACTURA" ("NUMFACTURA") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index HABITACION_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "C##PORTA"."HABITACION_PK" ON "C##PORTA"."HABITACION" ("IDHABITACION") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index HUESPED_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "C##PORTA"."HUESPED_PK" ON "C##PORTA"."HUESPED" ("RUTH") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index ORDENCOMPRA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "C##PORTA"."ORDENCOMPRA_PK" ON "C##PORTA"."ORDENCOMPRA" ("NUMORDEN") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index PRODUCTO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "C##PORTA"."PRODUCTO_PK" ON "C##PORTA"."PRODUCTO" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index TIPOHABITACION_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "C##PORTA"."TIPOHABITACION_PK" ON "C##PORTA"."TIPOHABITACION" ("TIPO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index TIPOSERVICIO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "C##PORTA"."TIPOSERVICIO_PK" ON "C##PORTA"."TIPOSERVICIO" ("NUMSERV") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index TIPOUSUARIO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "C##PORTA"."TIPOUSUARIO_PK" ON "C##PORTA"."TIPOUSUARIO" ("TIPOUSER") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index USUARIO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "C##PORTA"."USUARIO_PK" ON "C##PORTA"."USUARIO" ("RUT") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index FACTURA__IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "C##PORTA"."FACTURA__IDX" ON "C##PORTA"."FACTURA" ("ORDENCOMPRA_NUMORDEN") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index COMEDOR__IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "C##PORTA"."COMEDOR__IDX" ON "C##PORTA"."COMEDOR" ("DIASEMANA_NUMDIA") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Trigger CAMBIODEPRECIO
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "C##PORTA"."CAMBIODEPRECIO" 
      after update of PRECIOP
      on PRODUCTO
      for each row
   begin
insert into auditar values
         ('se ha modificado el precio' || :old.descripcion);
   end; 
/
ALTER TRIGGER "C##PORTA"."CAMBIODEPRECIO" ENABLE;
--------------------------------------------------------
--  DDL for Trigger FACTURA_TRIG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "C##PORTA"."FACTURA_TRIG" 
    BEFORE INSERT ON FACTURA
    FOR EACH ROW
BEGIN
    SELECT FACTURA_SEQ.NEXTVAL
    INTO :new.NUMFACTURA
    FROM DUAL;
END;


/
ALTER TRIGGER "C##PORTA"."FACTURA_TRIG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger HABITACION_TRIG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "C##PORTA"."HABITACION_TRIG" 
    BEFORE INSERT ON HABITACION
    FOR EACH ROW
BEGIN
    SELECT HABITACION_SEQ.NEXTVAL
    INTO :new.IDHABITACION
    FROM DUAL;
END;


/
ALTER TRIGGER "C##PORTA"."HABITACION_TRIG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ORDENCOMPRA_TRIG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "C##PORTA"."ORDENCOMPRA_TRIG" 
    BEFORE INSERT ON ORDENCOMPRA
    FOR EACH ROW
BEGIN
    SELECT ORDENCOMPRA_SEQ.NEXTVAL
    INTO :new.NUMORDEN
    FROM DUAL;
END;


/
ALTER TRIGGER "C##PORTA"."ORDENCOMPRA_TRIG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TR_ACTUALIZAR_PRECIO_PRODUCTO
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "C##PORTA"."TR_ACTUALIZAR_PRECIO_PRODUCTO" 
 before update of preciop
 on producto
 for each row
 begin
  if (:new.preciop>50) then
   :new.preciop:=floor(:new.preciop);
  end if;
  insert into control values(user,sysdate,:id,:old.preciop,:new.preciop);
 end tr_actualizar_precio_producto;
/
ALTER TRIGGER "C##PORTA"."TR_ACTUALIZAR_PRECIO_PRODUCTO" ENABLE;
--------------------------------------------------------
--  DDL for Trigger VALIDAPRECIO
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "C##PORTA"."VALIDAPRECIO" before insert on producto for each row
declare
--variables
begin
if :new.PRECIOP < 0 then
:new.PRECIOP := 0;
end if;

if :new.STOCK < 0 then
:new.STOCK :=0;
end if;

end;
/
ALTER TRIGGER "C##PORTA"."VALIDAPRECIO" ENABLE;
--------------------------------------------------------
--  Constraints for Table HUESPED
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."HUESPED" MODIFY ("RUTH" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."HUESPED" MODIFY ("NOMBREH" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."HUESPED" MODIFY ("HABITACION_IDHABITACION" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."HUESPED" ADD CONSTRAINT "HUESPED_PK" PRIMARY KEY ("RUTH")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table TIPOUSUARIO
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."TIPOUSUARIO" MODIFY ("TIPOUSER" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."TIPOUSUARIO" MODIFY ("NOMTIPO" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."TIPOUSUARIO" ADD CONSTRAINT "TIPOUSUARIO_PK" PRIMARY KEY ("TIPOUSER")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table TIPOSERVICIO
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."TIPOSERVICIO" MODIFY ("NUMSERV" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."TIPOSERVICIO" MODIFY ("NOMSERV" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."TIPOSERVICIO" ADD CONSTRAINT "TIPOSERVICIO_PK" PRIMARY KEY ("NUMSERV")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table DIASEMANA
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."DIASEMANA" MODIFY ("NUMDIA" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."DIASEMANA" MODIFY ("NOMDIA" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."DIASEMANA" ADD CONSTRAINT "DIASEMANA_PK" PRIMARY KEY ("NUMDIA")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table ORDENCOMPRA
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."ORDENCOMPRA" MODIFY ("NUMORDEN" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."ORDENCOMPRA" MODIFY ("CANTIDAD" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."ORDENCOMPRA" MODIFY ("PRECIOTOTAL" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."ORDENCOMPRA" MODIFY ("COSTOTOTAL" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."ORDENCOMPRA" MODIFY ("HABITACION_IDHABITACION" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."ORDENCOMPRA" ADD CONSTRAINT "ORDENCOMPRA_PK" PRIMARY KEY ("NUMORDEN")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table USUARIO
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."USUARIO" MODIFY ("RUT" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."USUARIO" MODIFY ("NOMBRE" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."USUARIO" MODIFY ("PASS" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."USUARIO" MODIFY ("EMAIL" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."USUARIO" MODIFY ("DIRECCION" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."USUARIO" MODIFY ("CIUDAD" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."USUARIO" MODIFY ("TELEFONO" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."USUARIO" MODIFY ("TIPOUSUARIO_TIPOUSER" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."USUARIO" ADD CONSTRAINT "USUARIO_PK" PRIMARY KEY ("RUT")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table HABITACION
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."HABITACION" MODIFY ("IDHABITACION" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."HABITACION" MODIFY ("ESTADOHA" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."HABITACION" MODIFY ("TIPOHABITACION_TIPO" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."HABITACION" ADD CONSTRAINT "HABITACION_PK" PRIMARY KEY ("IDHABITACION")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table TIPOHABITACION
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."TIPOHABITACION" MODIFY ("TIPO" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."TIPOHABITACION" MODIFY ("NOMBREHA" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."TIPOHABITACION" MODIFY ("PRECIO" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."TIPOHABITACION" ADD CONSTRAINT "TIPOHABITACION_PK" PRIMARY KEY ("TIPO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table PRODUCTO
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."PRODUCTO" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."PRODUCTO" MODIFY ("FECHAV" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."PRODUCTO" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."PRODUCTO" MODIFY ("PRECIOP" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."PRODUCTO" MODIFY ("STOCK" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."PRODUCTO" MODIFY ("STOCKCRITICO" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."PRODUCTO" ADD CONSTRAINT "PRODUCTO_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table COMEDOR
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."COMEDOR" MODIFY ("IDCOMEDOR" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."COMEDOR" MODIFY ("DESAYUNO" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."COMEDOR" MODIFY ("ALMUERZO" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."COMEDOR" MODIFY ("CENA" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."COMEDOR" MODIFY ("DIASEMANA_NUMDIA" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."COMEDOR" MODIFY ("TIPOSERVICIO_NUMSERV" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."COMEDOR" ADD CONSTRAINT "COMEDOR_PK" PRIMARY KEY ("IDCOMEDOR")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table FACTURA
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."FACTURA" MODIFY ("NUMFACTURA" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."FACTURA" MODIFY ("ORDENCOMPRA_NUMORDEN" NOT NULL ENABLE);
  ALTER TABLE "C##PORTA"."FACTURA" ADD CONSTRAINT "FACTURA_PK" PRIMARY KEY ("NUMFACTURA")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table COMEDOR
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."COMEDOR" ADD CONSTRAINT "COMEDOR_DIASEMANA_FK" FOREIGN KEY ("DIASEMANA_NUMDIA")
	  REFERENCES "C##PORTA"."DIASEMANA" ("NUMDIA") ENABLE;
  ALTER TABLE "C##PORTA"."COMEDOR" ADD CONSTRAINT "COMEDOR_TIPOSERVICIO_FK" FOREIGN KEY ("TIPOSERVICIO_NUMSERV")
	  REFERENCES "C##PORTA"."TIPOSERVICIO" ("NUMSERV") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table FACTURA
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."FACTURA" ADD CONSTRAINT "FACTURA_ORDENCOMPRA_FK" FOREIGN KEY ("ORDENCOMPRA_NUMORDEN")
	  REFERENCES "C##PORTA"."ORDENCOMPRA" ("NUMORDEN") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table HABITACION
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."HABITACION" ADD CONSTRAINT "HABITACION_TIPOHABITACION_FK" FOREIGN KEY ("TIPOHABITACION_TIPO")
	  REFERENCES "C##PORTA"."TIPOHABITACION" ("TIPO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table HUESPED
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."HUESPED" ADD CONSTRAINT "HUESPED_HABITACION_FK" FOREIGN KEY ("HABITACION_IDHABITACION")
	  REFERENCES "C##PORTA"."HABITACION" ("IDHABITACION") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ORDENCOMPRA
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."ORDENCOMPRA" ADD CONSTRAINT "ORDENCOMPRA_HABITACION_FK" FOREIGN KEY ("HABITACION_IDHABITACION")
	  REFERENCES "C##PORTA"."HABITACION" ("IDHABITACION") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table USUARIO
--------------------------------------------------------

  ALTER TABLE "C##PORTA"."USUARIO" ADD CONSTRAINT "USUARIO_TIPOUSUARIO_FK" FOREIGN KEY ("TIPOUSUARIO_TIPOUSER")
	  REFERENCES "C##PORTA"."TIPOUSUARIO" ("TIPOUSER") ENABLE;
