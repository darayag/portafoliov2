<%-- 
    Document   : editarHE
    Created on : 22-05-2019, 23:06:06
    Author     : the_e
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <head>
        <meta charset="ISO-8859-1">  
        <link href="csss/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/fav.png">
        <!-- Author Meta -->
        <meta name="author" content="CodePixar">
        <!-- Meta Description -->
        <meta name="description" content="">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->
        <title>Habitaciones</title>
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,500,600" rel="stylesheet">
        <!--
        CSS
        ============================================= -->
        <link rel="stylesheet" href="css/linearicons.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
    <body>
        <!-- Start Conexion Area -->
        <%
            //CONECTANOD A LA BASE DE DATOS:

            Class.forName("oracle.jdbc.OracleDriver").newInstance();
            Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "C##PORTA", "oracle");
            Statement stm = con.createStatement();
            int id = Integer.parseInt(request.getParameter("idhabitacion"));
            String Query = "select * from habitacion where idhabitacion=" + id;
            PreparedStatement ps;
            ResultSet rs = stm.executeQuery(Query);
            while (rs.next()) {
        %>
        <!-- End Conexion Area -->


        <div class="oz-body-wrap">
            <!-- Start Header Area -->
            <header class="default-header generic-header">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <div class="header-top d-flex justify-content-between align-items-center">
                            <div class="logo">
                                <a href="index.jsp"><img src="img/logo.png" alt=""></a>
                            </div>
                            <div class="main-menubar d-flex align-items-center">
                                <nav class="hide">
                                    <a href="galeriaHabitaciones.jsp">Fotos de Habitaciones</a>
                                    <a href="getAll.html">Lista de Usuarios  </a>
                                    <a href="getAlldp.html">Lista de Empresas </a>
                                    <a href="getAllhb.html">Lista de Habitaciones </a>
                                    <a href="http://localhost:8080/x/contacto/index.html">Contacto</a>
                                </nav>
                                <div class="menu-bar"><span class="lnr lnr-menu"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- End Header Area -->
            <!-- Start Banner Area -->

            <section class="generic-banner relative">


                <div class="overlay overlay-bg"></div>
                <div class="container">
                    <br>

                    <div class="row height align-items-center justify-content-center">
                        <h1>Editar Habitaciones </h1>

                        <div class="col-lg-10">
                            <div class="banner-content text-center">

                                <br>
                                <div class="container">


                                    <center>
                                        <form action="" method="POST" class="form-control" style="width: 500px; height: 400px">
                                            Numero Habitacion:
                                            <input type="text" readonly="" class="form-control" value="<%= rs.getInt("idhabitacion")%>"/> <br>
                                            Estado :
                                            <select  class="form-control" style="width: 250px;" id="estadoha" value="<%= rs.getString("estadoha")%>" name="estadoid" >
                                                <option>LIBRE</option> 
                                                <option>OCUPADO</option> 
                                            </select>
                                            <br>



                                            Tipo habitacion:
                                            <select  class="form-control" style="width: 250px;" id="tipohabitacion_tipohaid" value="<%= rs.getString("tipohabitacion_tipoha")%>" name="tipoid" >
                                                <option>Seleccione</option>
                                                <%

                                                    try {
                                                        String Query3 = "select * from tipoHabitacion";
                                                        Class.forName("oracle.jdbc.OracleDriver").newInstance();
                                                        Statement stm3 = con.createStatement();
                                                        ResultSet rs3 = stm3.executeQuery(Query3);
                                                        while (rs3.next()) {
                                                %>
                                                <option value="<%=rs3.getString("tipoha")%>"><%=rs3.getString("nombreha")%></option>
                                                <%
                                                        }

                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                        out.print("Error " + ex.getMessage());
                                                    }


                                                %>
                                            </select>


                                            <br>
                                            Empresa :

                                            <select  class="form-control" style="width: 250px;" id="empresa_rutid" value="<%= rs.getString("empresa_rut")%>" name="empresaid" >
                                                <option>Seleccione</option>
                                                <%

                                                    try {
                                                        String Query2 = "select * from empresa";
                                                        Class.forName("oracle.jdbc.OracleDriver").newInstance();
                                                        Statement stm2 = con.createStatement();
                                                        ResultSet rs2 = stm2.executeQuery(Query2);
                                                        while (rs2.next()) {
                                                %>
                                                <option value="<%=rs2.getString("rut")%>"><%=rs2.getString("rut")%></option>
                                                <%
                                                        }

                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                        out.print("Error " + ex.getMessage());
                                                    }

                                                %>
                                            </select>
                                            <br>



                                            <input type="submit" value="Guardar"/>
                                            <a href="getAllhb.html">Regresar</a>
                                        </form>

                                        <%}%>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
            <!-- Start Feature Area -->

            <!-- Start Feature Area -->
            <!-- About Generic Start -->

            <!-- Start Service Area -->

            <!-- End Service Area -->
            <!-- Start Footer Area -->

            <!-- End Footer Area -->
        </div>
        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>


<%
    String esta, tipoh, empr;
    esta = request.getParameter("estadoid");
    tipoh = request.getParameter("tipoid");
    empr = request.getParameter("empresaid");

    if (esta != null && tipoh != null && empr != null) {
        ps = con.prepareStatement("update habitacion set estadoha='" + esta + "', tipohabitacion_tipoha='" + tipoh + "', empresa_rut='" + empr + "'where idhabitacion=" + id);
        ps.executeUpdate();
        response.sendRedirect("getAllhb.html");
    }


%>
