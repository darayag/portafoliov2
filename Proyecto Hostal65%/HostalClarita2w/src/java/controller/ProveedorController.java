/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Proveedor;
import model.EmpleadoModel;
import model.ProveedorModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Diodoros
 */

@Controller
public class ProveedorController {
     @RequestMapping(value = "getAllpr", method = RequestMethod.GET)
    public String getAllem(Model m) {

        ProveedorModel model = new ProveedorModel();
        m.addAttribute("lst", model.getAll());
        return "datapr";
    }

    //remover
    @RequestMapping(value = "removepr", method = RequestMethod.GET)
    public String removedp(@RequestParam(value = "rutp") String rutp, Model m) {

        String bd = new String(rutp);
        ProveedorModel model = new ProveedorModel();
        //get EMpleados bases objct id        m.addAttribute("lst",model.getAll());
        Proveedor pr = new Proveedor();
        pr = model.getEmpleado(bd);
        model.remove(pr);
        // m.addAttribute("lst",e);       

        return "redirect:getAllpr.html";
    }

    //create
    @RequestMapping(value = "crearpr", method = RequestMethod.GET)
    public String createdp(Model m) {
        //  java.math.BigDecimal bd=new java.math.BigDecimal(String.valueOf(id));
        Proveedor dp = new Proveedor();

        m.addAttribute("dp", dp);

        return "crearpr";
    }

    @RequestMapping(value = "addpr", method = RequestMethod.POST)
    public String createdp(@ModelAttribute(value = "Proveedor") Proveedor dp) {

        ProveedorModel model = new ProveedorModel();
        model.create(dp);
        return "redirect:getAllpr.html";
    }

}
