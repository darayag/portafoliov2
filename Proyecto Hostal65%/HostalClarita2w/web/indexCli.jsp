<%-- 
    Document   : indexCli
    Created on : 21-05-2019, 1:40:15
    Author     : the_e
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HttpSession objsesion = request.getSession(false);
    //Se cambio todo esto de email a correo <- ver LoginUsuarioController donde inicio la session.
    String correo = (String) objsesion.getAttribute("correo");

    if (correo == null) {
        response.sendRedirect("login.jsp");
    }
%>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/fav.png">
        <!-- Author Meta -->
        <meta name="author" content="CodePixar">
        <!-- Meta Description -->
        <meta name="description" content="">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->
        <title>Hostel Clarita</title>

        <link href="https://fonts.googleapis.com/css?family=Poppins:100,300,500" rel="stylesheet">
        <!--
        CSS
        ============================================= -->
        <link rel="stylesheet" href="css/linearicons.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <div class="oz-body-wrap">
            <!-- Start Header Area -->
            <header class="default-header">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <div class="header-top d-flex justify-content-between align-items-center">
                            <div class="logo">
                                <a href="index.jsp"><img src="img/logo.png" alt=""></a>
                                 <h6><li><a href="login.jsp" style="opacity: 1;"><img src="imagenes/salir.png" alt=""></a></li></h6>

                            </div>
                            <div class="main-menubar d-flex align-items-center">
                                <nav class="hide">
                                 <a href="index.jsp">Inicio</a>
                                    <a href="galeriaHabitaciones.jsp">Fotos de Habitaciones</a>
                                    <a href="http://localhost:8080/x/contacto/index.html">Contacto</a>
                                    
                                </nav>
                                <div class="menu-bar"><span class="lnr lnr-menu"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- End Header Area -->
            <!-- Start Banner Area -->
            <section class="banner-area relative">
                <div class="overlay overlay-bg"></div>
                <div class="container">
                    <div class="row fullscreen align-items-center justify-content-center">
                        <div class="col-lg-10">
                            <div class="banner-content text-center">
                                <br> <br/>  <br> <br/><br> <br/>  <br> <br/><br/>  
                                <h1 class="text-uppercase text-white">Bienvenido a Nuestro Hostal<br>

                                </h1>
                                <a href="#" class="genric-btn primary circle arrow"><h5>Bienvenido : <% out.println(correo);%></h5><span class="lnr lnr-arrow-right"></span></a>
                            </div>
                            <br> <br/> <br> <br/> <br> <br/>
                            <a><img src="img/1h.jpg" width="300" height="300" alt=""></a> <a><img src="img/2h.jpg" width="300" height="300" alt=""></a> <a><img src="img/3 h.jpg" width="300" height="300" alt=""></a>
                            <br> <br/> <br> <br/> <br> <br/> <br> <br/>
                            <div class="footer-bottom d-flex justify-content-between align-items-center flex-wrap">
                                <p class="footer-text m-0">Convenio y servicios especiales para empresas <i class="fa fa-heart-o" aria-hidden="true"></i> </p>
                                <div class="footer-social d-flex align-items-center">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-dribbble"></i></a>
                                    <a href="#"><i class="fa fa-behance"></i></a>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </section>
            <!-- End Banner Area -->
            <!-- Start Team Force Area -->
            <section class="team-force-area pt-60">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6">
                            <img class="img-fluid" src="img/5h.jpg" alt="">
                            <br><br/>
                        </div>
                        <div class="col-lg-6">
                            <div class="story-content">
                                <h2>Tipo de hacitaciones</h2>
                                <p class="mt-30 mb-30">El hostel Clarita tiene a la disposicion de usted lo mejor en habitaciones para su hospitalidad ,esta sera dividad en 3 tipos de habitaciones :Habitaciòn Sencilla :Habitaciòn Matrimonial y Suite.</p>
                                <a href="habitacion.jsp" class="genric-btn primary circle arrow">Tipo de Habitaciones <span class="lnr lnr-arrow-right"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Team Force Area -->
            <!-- Start Feature Area -->
            <section class="feature-area pt-80 pb-40">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="single-feature d-flex justify-content-between align-items-top">
                                <div class="icon">
                                    <span class="lnr lnr-laptop-phone"></span>
                                </div>
                                <div class="desc">
                                    <h2 class="text-uppercase">Reservaciones</h2>
                                    <p>
                                        El sistema de reservaciones esta creado y progresivamente  actualizado para poder funcionar en cualquier dispositivo sea via computacional o movial , para brindar un buen servicio para nuestros clientes.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="single-feature d-flex justify-content-between align-items-top">
                                <div class="icon">
                                    <span class="lnr lnr-sun"></span>
                                </div>
                                <div class="desc">
                                    <h2 class="text-uppercase">Vistas Maravillosas</h2>
                                    <p>
                                        Nuestros hostal tiene la mejor ubicacion posible ,puede disfrutar de la naturaleza en su maxima expresion.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="single-feature d-flex justify-content-between align-items-top">
                                <div class="icon">
                                    <span class="lnr lnr-picture"></span>
                                </div>
                                <div class="desc">
                                    <h2 class="text-uppercase">Seccion Imagenes </h2>
                                    <p>
                                        Visita la seccion de imagenes de todas las habitaciones de nuestro hostal que estan disponible para los usuarios.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="single-feature d-flex justify-content-between align-items-top">
                                <div class="icon">
                                    <span class="lnr lnr-laptop"></span>
                                </div>
                                <div class="desc">
                                    <h2 class="text-uppercase">Retina Ready Graphics</h2>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="single-feature d-flex justify-content-between align-items-top">
                                <div class="icon">
                                    <span class="lnr lnr-camera-video"></span>
                                </div>
                                <div class="desc">
                                    <h2 class="text-uppercase">Seguridad</h2>
                                    <p>
                                        Vigilancia y total seguridad dentro y fuera del hotel ,para que los clientes puedan estar siempre resguardados y protegidos ante cualquier peligro externo o interno.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="single-feature d-flex justify-content-between align-items-top">
                                <div class="icon">
                                    <span class="lnr lnr-rocket"></span>
                                </div>
                                <div class="desc">
                                    <h2 class="text-uppercase">Powerful Performance</h2>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <!-- Start Feature Area -->
            <!-- Start Video Area -->
            <section class="video-area pt-40 pb-40">
                <div class="overlay overlay-bg"></div>
                <div class="container">
                    <div class="video-content">
                        <a href="https://www.youtube.com/watch?v=VAXg78MKJcM" class="play-btn"><img src="img/play-btn.png" alt=""></a>
                        <div class="video-desc">
                            <h3 class="h2 text-white text-uppercase">Being unique is the preference now-a-days</h3>
                            <h4 class="text-white">Youtube video will appear in popover</h4>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Start Video Area -->
            <!-- Start Quote Area -->
            <section class="quote-area pt-60 pb-60">
                <div class="container">
                    <div class="row">
                        <div class="counter-left col-lg-4">
                            <h2>10</h2>
                            <p>Opiniones de Clientes</p>
                        </div>
                        <div class="active-bottle-carousel col-lg-8 align-self-center">
                            <div class="item carousel-content slider-right">
                                <p>
                                    “El ambiente muy grato y tranquilo ,siento que es lo mismo que quedarme en un hotel de 5 estrellas .
                                </p>
                                <span class="slide-name text-uppercase">Oscar Soto, Wallmart.</span>
                            </div>
                            <div class="item carousel-content slider-right">
                                <p>
                                    “Exactamente lo que buscabamos como trabajadores de empresa un lugar decente ,tranquilo y adecuado a nuestros requerimientos.
                                </p>
                                <span class="slide-name text-uppercase">Fabricio Wiens, Ripley.</span>
                            </div>
                            <div class="item carousel-content slider-right">
                                <p>
                                    “Lejos el mejor servicio de hostales al que he tenido el honor de ingresar ,todo los servicio excelentes y que decir de su pagina web un 10.
                                </p>
                                <span class="slide-name text-uppercase">Felipe Ambrosio, Entel.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Start Quote Area -->
            <!-- Start Service Area -->
            <section class="service-area no-gutters">
                <div class="container-fluid">
                    <div class="row">
                        <div class="image-left col-lg-5 col-sm-4">
                            <div class="overlay overlay-bg"></div>
                            <img class="img-fluid" src="img/service.jpg" alt="">
                        </div>
                        <div class="service-right col-lg-6 col-sm-8 text-center align-self-center">
                            <div class="row no-gutters">
                                <div class="single-service pt-40 pb-40 bbr bbr-b">
                                    <span class="lnr lnr-laptop-phone"></span>
                                    <h6>Atencion Web</h6>
                                </div>
                                <div class="single-service pt-40 pb-40 bbr bbr-b">
                                    <span class="lnr lnr-magic-wand"></span>
                                    <h6>Diseño Grato</h6>
                                </div>
                                <div class="single-service pt-40 pb-40 bbr-b">
                                    <span class="lnr lnr-cog"></span>
                                    <h6>Orgnazicion </h6>
                                </div>
                                <div class="single-service pt-40 pb-40 bbr">
                                    <span class="lnr lnr-user"></span>
                                    <h6>Soporte</h6>
                                </div>
                                <div class="single-service pt-40 pb-40 bbr">
                                    <span class="lnr lnr-poop"></span>
                                    <h6>MOJON</h6>
                                </div>

                                <div class="single-service pt-40 pb-40">
                                    <span class="lnr lnr-gift"></span>
                                    <h6>Regalo</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Service Area -->
            <!-- Start Conatct- Area -->
            <section class="contact-area pt-80 pb-80">
                <div class="container">
                    <div class="row">
                        <div class="single-contact col-lg-3">
                            <h2 class="text-uppercase">Visite Nuestro Hostal</h2>
                            <p>
                                Avenida no tengo idea
                            </p>
                        </div>
                        <div class="single-contact col-lg-3">
                            <h2 class="text-uppercase">Reserve Via Telefonica</h2>
                            <p>
                                Phone 01: 012-6532-568-9746 <br>
                                Phone 02: 012-6532-568-9748 <br>
                                FAX: 02-6532-568-746
                            </p>
                        </div>
                        <div class="single-contact col-lg-3">
                            <h2 class="text-uppercase">Mandenos Un Email </h2>
                            <p>
                                hello@colorlib.com <br>
                                mainhelpinfo@colorlib.com <br>
                                infohelp@colorlib.com
                            </p>
                        </div>
                        <div class="single-contact col-lg-3">
                            <h2 class="text-uppercase">Soporte</h2>
                            <p>
                                support@colorlib.com <br>
                                emergencysupp@colorlib.com <br>
                                extremesupp@colorlib.com
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Conatct- Area -->
            <!-- Strat Footer Area -->
            <footer class="section-gap">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="subcribe-form col-lg-6">
                            <div id="mc_embed_signup">
                                <form target="_blank" novalidate action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&id=92a4423d01" method="get" class="subscription relative">
                                    <input type="email" name="EMAIL" placeholder="Enter Email To Our Newsletter" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email To Our Newsletter'" required>
                                    <div style="position: absolute; left: -5000px;">
                                        <input type="text" name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="">
                                    </div>
                                    <button class="primary-btn white-bg d-inline-flex align-items-center"><span class="mr-10">Get Started</span><span class="lnr lnr-arrow-right"></span></button>
                                    <div class="info"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row pt-60">
                        <div class="col-lg-3 col-sm-6">
                            <div class="single-footer-widget">
                                <h6 class="text-uppercase mb-20">Servicios Top</h6>
                                <ul class="footer-nav">
                                    <li><a href="#">Managed Website</a></li>
                                    <li><a href="#">Manage Reputation</a></li>
                                    <li><a href="#">Power Tools</a></li>
                                    <li><a href="#">Marketing Service</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="single-footer-widget">
                                <h6 class="text-uppercase mb-20">Navegacion</h6>
                                <ul class="footer-nav">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Main Features</a></li>
                                    <li><a href="#">Offered Services</a></li>
                                    <li><a href="#">Latest Portfolio</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="single-footer-widget">
                                <h6 class="text-uppercase mb-20">Compare</h6>
                                <ul class="footer-nav">
                                    <li><a href="#">Works & Builders</a></li>
                                    <li><a href="#">Works & Wordpress</a></li>
                                    <li><a href="#">Works & Templates</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6">
                            <div class="single-footer-widget">
                                <h6 class="text-uppercase mb-20">Instragram Feed</h6>
                                <ul class="instafeed d-flex flex-wrap">
                                    <li><img src="img/i1.jpg" alt=""></li>
                                    <li><img src="img/i2.jpg" alt=""></li>
                                    <li><img src="img/i3.jpg" alt=""></li>
                                    <li><img src="img/i4.jpg" alt=""></li>
                                    <li><img src="img/i5.jpg" alt=""></li>
                                    <li><img src="img/i6.jpg" alt=""></li>
                                    <li><img src="img/i7.jpg" alt=""></li>
                                    <li><img src="img/i8.jpg" alt=""></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footer-bottom d-flex justify-content-between align-items-center flex-wrap">
                        <p class="footer-text m-0">Copyright &copy; 2019 All rights reserved   <i class="fa fa-heart-o" aria-hidden="true"></i> </p>
                        <div class="footer-social d-flex align-items-center">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-dribbble"></i></a>
                            <a href="#"><i class="fa fa-behance"></i></a>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End Footer Area -->

        </div>

        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
