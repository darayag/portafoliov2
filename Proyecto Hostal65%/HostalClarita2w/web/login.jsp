<%-- 
    Document   : login
    Created on : 13-nov-2018, 23:10:31
    Author     : Efren
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<html >
    <head>
        <meta charset="UTF-8">
        <title>Hostel Clarita | Inicio de sesión</title>
        <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">


        <link rel="stylesheet" href="CSSLogin/css/style.css">
    </head>

    <body>
        <div class="form">

         

            

                 <div id="login">   
                    <h1>¡Bienvenido!</h1>

                    <form action="LoginUsuarioController" method="post">

                        <div class="field-wrap">
                            <label>
                                Correo electrónico<span class="req">*</span>
                            </label>
                            <input type="correo"required autocomplete="off" name="correo"/>
                        </div>

                        <div class="field-wrap">
                            <label>
                                Contraseña<span class="req">*</span>
                            </label>
                            <input type="password"required autocomplete="off" name="pass"/>
                        </div>

                        <p class="forgot"><a href="#">Recuperar contraseña</a></p>

                        <button class="button button-block"/>Iniciar sesión</button>

                    </form>

                </div>
            </div><!-- tab-content -->

        </div> <!-- /form -->
        <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script  src="CSSLogin/js/index.js"></script>

    </body>
</html>