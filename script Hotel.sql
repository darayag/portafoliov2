-- MySQL Script generated by MySQL Workbench
-- Mon Mar 25 15:21:56 2019
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema hotel
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema hotel
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `hotel` DEFAULT CHARACTER SET utf8 ;
USE `hotel` ;

-- -----------------------------------------------------
-- Table `hotel`.`Huesped`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hotel`.`Huesped` ;

CREATE TABLE IF NOT EXISTS `hotel`.`Huesped` (
  `rutH` VARCHAR(45) NOT NULL,
  `nombreH` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`rutH`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hotel`.`Comedor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hotel`.`Comedor` ;

CREATE TABLE IF NOT EXISTS `hotel`.`Comedor` (
  `tipoPlato` CHAR NOT NULL,
  `precioPl` INT NOT NULL,
  `tipoServicioPl` CHAR NOT NULL,
  PRIMARY KEY (`tipoPlato`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hotel`.`TipoHabitacion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hotel`.`TipoHabitacion` ;

CREATE TABLE IF NOT EXISTS `hotel`.`TipoHabitacion` (
  `idTipo` INT NOT NULL,
  `nombreHabitacion` CHAR NOT NULL,
  PRIMARY KEY (`idTipo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hotel`.`ordenDeCompra`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hotel`.`ordenDeCompra` ;

CREATE TABLE IF NOT EXISTS `hotel`.`ordenDeCompra` (
  `idNumero` INT NOT NULL AUTO_INCREMENT,
  `fecha` DATE NOT NULL,
  `idHabitacion` INT NOT NULL,
  `totalCompra` INT NOT NULL,
  `ordenDeCompracol` INT NOT NULL,
  PRIMARY KEY (`idNumero`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hotel`.`Habitacion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hotel`.`Habitacion` ;

CREATE TABLE IF NOT EXISTS `hotel`.`Habitacion` (
  `idHabitacion` INT NOT NULL AUTO_INCREMENT,
  `estadoHa` TINYINT NOT NULL,
  `tipoCama` CHAR NOT NULL,
  `accesoriosHa` VARCHAR(45) NOT NULL,
  `precioHa` INT NOT NULL,
  `fechaEntradaHa` DATE NOT NULL,
  `fechaSalidaHa` DATE NOT NULL,
  `ordenDeCompra_idNumero` INT NOT NULL,
  `TipoHabitacion_idTipo` INT NOT NULL,
  `Huesped_rutH` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idHabitacion`, `ordenDeCompra_idNumero`, `TipoHabitacion_idTipo`, `Huesped_rutH`),
  INDEX `fk_Habitacion_ordenDeCompra_idx` (`ordenDeCompra_idNumero` ASC) VISIBLE,
  INDEX `fk_Habitacion_TipoHabitacion1_idx` (`TipoHabitacion_idTipo` ASC) VISIBLE,
  INDEX `fk_Habitacion_Huesped1_idx` (`Huesped_rutH` ASC) VISIBLE,
  CONSTRAINT `fk_Habitacion_ordenDeCompra`
    FOREIGN KEY (`ordenDeCompra_idNumero`)
    REFERENCES `hotel`.`ordenDeCompra` (`idNumero`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Habitacion_TipoHabitacion1`
    FOREIGN KEY (`TipoHabitacion_idTipo`)
    REFERENCES `hotel`.`TipoHabitacion` (`idTipo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Habitacion_Huesped1`
    FOREIGN KEY (`Huesped_rutH`)
    REFERENCES `hotel`.`Huesped` (`rutH`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hotel`.`Factura`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hotel`.`Factura` ;

CREATE TABLE IF NOT EXISTS `hotel`.`Factura` (
  `numFactura` INT NOT NULL AUTO_INCREMENT,
  `ordenDeCompra_idNumero` INT NOT NULL,
  PRIMARY KEY (`numFactura`, `ordenDeCompra_idNumero`),
  INDEX `fk_Factura_ordenDeCompra1_idx` (`ordenDeCompra_idNumero` ASC) VISIBLE,
  CONSTRAINT `fk_Factura_ordenDeCompra1`
    FOREIGN KEY (`ordenDeCompra_idNumero`)
    REFERENCES `hotel`.`ordenDeCompra` (`idNumero`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hotel`.`TipoUser`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hotel`.`TipoUser` ;

CREATE TABLE IF NOT EXISTS `hotel`.`TipoUser` (
  `idTipoUser` INT NOT NULL,
  PRIMARY KEY (`idTipoUser`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hotel`.`Usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hotel`.`Usuario` ;

CREATE TABLE IF NOT EXISTS `hotel`.`Usuario` (
  `Rut` VARCHAR(45) NOT NULL,
  `Nombre` VARCHAR(45) NOT NULL,
  `Pass` VARCHAR(45) NOT NULL,
  `Direccion` VARCHAR(45) NOT NULL,
  `Ciudad` VARCHAR(45) NOT NULL,
  `Telefono` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(45) NOT NULL,
  `TipoUser_idTipoUser` INT NOT NULL,
  PRIMARY KEY (`Rut`, `TipoUser_idTipoUser`),
  INDEX `fk_Usuario_TipoUser1_idx` (`TipoUser_idTipoUser` ASC) VISIBLE,
  CONSTRAINT `fk_Usuario_TipoUser1`
    FOREIGN KEY (`TipoUser_idTipoUser`)
    REFERENCES `hotel`.`TipoUser` (`idTipoUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
