<%-- 
    Document   : data
    Created on : 15-05-2019, 22:48:20
    Author     : Diodoros
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <head>
        <meta charset="ISO-8859-1">  
        <link href="csss/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/fav.png">
        <!-- Author Meta -->
        <meta name="author" content="CodePixar">
        <!-- Meta Description -->
        <meta name="description" content="">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->
        <title>Lista Usuarios</title>
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,500,600" rel="stylesheet">
        <!--
        CSS
        ============================================= -->
        <link rel="stylesheet" href="css/linearicons.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
    <body>
        <div class="oz-body-wrap">
            <!-- Start Header Area -->
            <header class="default-header generic-header">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <div class="header-top d-flex justify-content-between align-items-center">
                            <div class="logo">
                                <a href="index.jsp"><img src="img/logo.png" alt=""></a>
                            </div>
                            <br>
                            <div class="main-menubar d-flex align-items-center">
                                <nav class="hide">
                                    <a href="getAll.html">Lista de Usuarios  </a>
                                    <a href="getAllem.html">Lista de Empleados </a>
                                    <a href="getAlldp.html">Lista de Empresas </a>
                                     <a href="getAllpr.html">Lista de Proveedores </a>
                                    <a href="getAllhb.html">Lista de Habitaciones </a> 
                                    <a href="getAllcm.html">Lista de Comedor </a>
                                   

                                </nav>
                                <div class="menu-bar"><span class="lnr lnr-menu"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- End Header Area -->
            <!-- Start Banner Area -->


            <section class="generic-banner relative">


                <div class="overlay overlay-bg"></div>
                <div class="container">


                    <div class="row height align-items-center justify-content-center">
                        <h1>Usuarios Registrados</h1>

                        <div class="col-lg-10">
                            <div class="banner-content text-center">

                                <br>
                                <div class="container">

                                    <table class="table table-bordered">
                                        <tr>
                                            <th class=text-center">Correo</th>
                                            <th class=text-center">Contraseña</th>
                                            <th class=text-center">Tipo Usuario</th>           
                                        </tr>

                                        <c:forEach items="${lst}" var="u">
                                            <tr>
                                                <td>${u.correo}</td>
                                                <td>${u.pass}</td>
                                                <td>${u.tipousuario.iduser}</td>       
                                                <td>
                                                    <a href="remove.html?correo=${u.correo}" class="btn btn-danger btn-sm">Eliminar</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                    <a class="btn btn-success btn-lg" href="crear.html">Nuevo Usuario</a>
                                </div>
                                <br>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Start Feature Area -->

            <!-- Start Feature Area -->
            <!-- About Generic Start -->

            <!-- Start Service Area -->

            <!-- End Service Area -->
            <!-- Start Footer Area -->

            <!-- End Footer Area -->
        </div>
        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
