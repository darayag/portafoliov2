/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import entity.Tipousuario;
import entity.Usuario;
import model.UsuarioModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;

/**
 *
 * @author Diodoros
 */
@Controller
public class UsuarioController {

    @RequestMapping("index")
    public ModelAndView redireccio() {

        ModelAndView MV = new ModelAndView();
        MV.setView("index");
        //     MV.addObject("mensaje","Hola soy un mensaje desde copntrolador");

        return MV;
    }

    //Listar
    @RequestMapping(value = "getAll", method = RequestMethod.GET)
    public String getAll(Model m) {

        UsuarioModel model = new UsuarioModel();
        m.addAttribute("lst", model.getAllUsuario());
        return "data";
    }

    //remover
    @RequestMapping(value = "remove", method = RequestMethod.GET)
    public String remove(@RequestParam(value = "correo") String correo, Model m) {

        String bd = new String(correo);
        UsuarioModel model = new UsuarioModel();
        Usuario u = new Usuario();
        u = model.getUsuario(bd);
        model.remove(u);

        return "redirect:getAll.html";
    }

    //crear
    @RequestMapping(value = "crear", method = RequestMethod.GET)
    public String create(Model m) {
        //  java.math.BigDecimal bd=new java.math.BigDecimal(String.valueOf(id));

        Usuario u = new Usuario();

        m.addAttribute("u", u);

        return "create";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String create(@ModelAttribute(value = "Usuario") Usuario u) {

 
        UsuarioModel model = new UsuarioModel();
        model.create(u);
        return "redirect:getAll.html";
    }

    
    
    
}
