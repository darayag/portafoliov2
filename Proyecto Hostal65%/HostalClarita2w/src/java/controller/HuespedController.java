/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Huesped;
import model.HuespedModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Diodoros
 */
@Controller
public class HuespedController {

    @RequestMapping(value = "getAllhu", method = RequestMethod.GET)
    public String getAllem(Model m) {

        HuespedModel model = new HuespedModel();
        m.addAttribute("lst", model.getAll());
        return "datahu";
    }

    //remover
    @RequestMapping(value = "removehu", method = RequestMethod.GET)
    public String removedp(@RequestParam(value = "ruth") String ruth, Model m) {

        String bd = new String(ruth);
        HuespedModel model = new HuespedModel();
        //get EMpleados bases objct id        m.addAttribute("lst",model.getAll());
        Huesped hu = new Huesped();
        hu = model.getHuesped(bd);
        model.remove(hu);
        // m.addAttribute("lst",e);       

        return "redirect:getAllhu.html";
    }

    //create
    @RequestMapping(value = "crearhu", method = RequestMethod.GET)
    public String createdp(Model m) {
        //  java.math.BigDecimal bd=new java.math.BigDecimal(String.valueOf(id));
        Huesped dp = new Huesped();

        m.addAttribute("dp", dp);

        return "crearhu";
    }

    @RequestMapping(value = "addhu", method = RequestMethod.POST)
    public String createdp(@ModelAttribute(value = "Huesped") Huesped dp) {

        HuespedModel model = new HuespedModel();
        model.create(dp);
        return "redirect:getAllhu.html";
    }

}
