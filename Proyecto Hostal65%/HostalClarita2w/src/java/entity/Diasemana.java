package entity;
// Generated 15-05-2019 22:13:08 by Hibernate Tools 4.3.1


import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Diasemana generated by hbm2java
 */
@Entity
@Table(name="DIASEMANA"
    ,schema="C##PORTA"
)
public class Diasemana  implements java.io.Serializable {


     private BigDecimal diaid;
     private String nombredia;
     private Set<Comedor> comedors = new HashSet<Comedor>(0);

    public Diasemana() {
    }

	
    public Diasemana(BigDecimal diaid, String nombredia) {
        this.diaid = diaid;
        this.nombredia = nombredia;
    }
    public Diasemana(BigDecimal diaid, String nombredia, Set<Comedor> comedors) {
       this.diaid = diaid;
       this.nombredia = nombredia;
       this.comedors = comedors;
    }
   
     @Id 

    
    @Column(name="DIAID", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getDiaid() {
        return this.diaid;
    }
    
    public void setDiaid(BigDecimal diaid) {
        this.diaid = diaid;
    }

    
    @Column(name="NOMBREDIA", nullable=false, length=192)
    public String getNombredia() {
        return this.nombredia;
    }
    
    public void setNombredia(String nombredia) {
        this.nombredia = nombredia;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="diasemana")
    public Set<Comedor> getComedors() {
        return this.comedors;
    }
    
    public void setComedors(Set<Comedor> comedors) {
        this.comedors = comedors;
    }




}


