/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Comedor;
import entity.Diasemana;
import entity.Empleado;
import model.ComedorModel;
import model.EmpleadoModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Diodoros
 */

@Controller
public class ComedorController {

    @RequestMapping(value = "getAllcm", method = RequestMethod.GET)
    public String getAlldp(Model m) {
        
        ComedorModel model = new ComedorModel();
        m.addAttribute("lst", model.getAll());
        return "datacm";
    }
    
    @RequestMapping(value = "editcm", method = RequestMethod.GET)
    public String editcm(@RequestParam(value = "idcomedor") int idcomedor, Model m) {
        java.math.BigDecimal bd = new java.math.BigDecimal(String.valueOf(idcomedor));
        ComedorModel model = new ComedorModel();
        
        Comedor c = new Comedor();
        Empleado em = new Empleado();
        EmpleadoModel hbModel = new EmpleadoModel();
        c = model.getComedor(bd);
        em = hbModel.getEmpleado(c.getEmpleado().getIdempleado());
        
        m.addAttribute("c", c);
        //m.addAttribute("d", dp);
        //  m.addAttribute("d",p.getDepartamento());
        
        return "editcm";
    }
    
    @RequestMapping(value = "updatecm", method = RequestMethod.POST)
    public String updatecm(@ModelAttribute(value = "Comedor") Comedor cm) {
        
        ComedorModel model = new ComedorModel();
        Comedor aux = new Comedor();
        aux = model.getComedor(cm.getIdcomedor());
        
        cm.setDiasemana(cm.getDiasemana());
        cm.setDesayuno(cm.getDesayuno());
        cm.setAlmuerzo(cm.getAlmuerzo());
        cm.setCena(cm.getCena());
        

        // aux.setDepartamento(dp);
        model.edit(cm);
        return "redirect:getAllcm.html";
    }

    //remover
    @RequestMapping(value = "removecm", method = RequestMethod.GET)
    public String removedp(@RequestParam(value = "idcomedor") int idcomedor, Model m) {
        
        java.math.BigDecimal bd = new java.math.BigDecimal(String.valueOf(idcomedor));
        ComedorModel model = new ComedorModel();
        //get EMpleados bases objct id        m.addAttribute("lst",model.getAll());
        Comedor c = new Comedor();
        c = model.getComedor(bd);
        model.remove(c);
        // m.addAttribute("lst",e);       

        return "redirect:getAllcm.html";
    }

    //create
    @RequestMapping(value = "crearcm", method = RequestMethod.GET)
    public String createdp(Model m) {
        //  java.math.BigDecimal bd=new java.math.BigDecimal(String.valueOf(id));

        Comedor cm = new Comedor();
        
        m.addAttribute("cm", cm);
        
        return "crearcm";
    }
    
    @RequestMapping(value = "addcm", method = RequestMethod.POST)
    public String createdp(@ModelAttribute(value = "Comedor") Comedor c) {
        
        ComedorModel model = new ComedorModel();
        model.create(c);
        return "redirect:getAllcm.html";
    }
    
}
